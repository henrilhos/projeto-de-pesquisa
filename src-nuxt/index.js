const { Nuxt, Builder } = require('nuxt')

function initialize (app) {
  if (process.argv[2] === 'api') {
    console.log('*** Skipping loading NuxtJs...')
  } else {
    console.log('** Loading NuxtJs...')
    let nuxtConfig = require('../nuxt.config.js')
    nuxtConfig.dev = (process.env.NODE_ENV !== 'production')

    const nuxt = new Nuxt(nuxtConfig)
    app.use(nuxt.render)

    if (nuxtConfig.dev) {
      console.log('Running NuxtJs in development mode...')
      const builder = new Builder(nuxt)
      builder.build()
        .catch((error) => {
          console.error(error)
          process.exit(1)
        })
    } else {
      console.log('Running NuxtJs in production mode...')
    }
  }
}

// se for passado o parâmetro 'api' não carrega o nuxt
module.exports = { initialize }
