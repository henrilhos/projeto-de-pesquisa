# Projeto de Pesquisa
Gerador de projetos de pesquisa para a Universidade Católica de Santa Catarina, desenvolvido na Fábrica de Software do campus de Joinville

**Alunos:** Bruno Henrique Junker, Henrique de Castilhos, Henrique Lorenzini e Jessica Amarante

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```
