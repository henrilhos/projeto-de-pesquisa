const Router = require('express').Router
const router = Router()
const projectBusiness = require('../business/project')

router.post('/newProject', (req, res) => {
  var projectData = req.body.project

  projectBusiness.createProject(projectData)
    .then(r => { res.json(r) })
    .catch(e => { res.json(null); console.log('** Error creating a project:', e) })
})

module.exports = router