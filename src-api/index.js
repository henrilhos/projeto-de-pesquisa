const Router = require('express').Router
const modules = [
  initializeBackend,
  initializeRoutes
]

function initialize (app) {
  modules.forEach(f => f(app))
}

function initializeBackend (app) {
  console.log('Initializing back-end API services...')

  const models = require('./model')
  console.log('Models loaded...')
}

function initializeRoutes (app) {
  const router = Router()
  console.log('Initializing routes...')

  router.use('/project', require('./routes/project'))
  console.log('Project loaded...')

  app.use('/api', router)
}

module.exports = { initialize }
