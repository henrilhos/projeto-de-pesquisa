const mongoose = require('mongoose')
const validators = require('.validators.js')
var Schema = mongoose.Schema

module.export = {
  name: 'Campus',
  definition: {
    enum: Number,
    description: String
  },
  options: {
    strict: false,
    collection: 'campus'
  } 
}