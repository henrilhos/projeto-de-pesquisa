const mongoose = require('mongoose')
const validators = require('./validators.js')
var Schema = mongoose.Schema

module.exports = {
  name: 'Work',
  definition: {
    title: String,
    author: String,
    publisher: String
  },
  options: {
    strict: false,
    collection: 'work'
  }
}