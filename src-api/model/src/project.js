const mongoose = require('mongoose')
const validators = require('./validators.js')
var Schema = mongoose.Schema

module.exports = {
  name: 'Project',
  definition: {
    projectTitle: String,
    presentedByTeacher: Boolean,
    resume: String,
    introduction: String,
    problem: String,
    justification: String,
    objective: String,
    method: String,
    submissionDate: String
  },
  options: {
    strict: false,
    collection: 'project'
  }
}